!/bin/bash
export HTTPS=false
cd /home/ubuntu/app/lms/app

username=`echo $USER`

if [ "$username" == "ubuntu" ]; then
        echo "Starting as ubuntu user!"
else
        echo "This script can only be run as an ubuntu user. Exiting. $USER"
        echo $USER
        exit 2
fi
export NODE_ENV=production
export HTTPS=false
export MONGODB_URL="mongodb://heroku:jgvNgPrRadAmCVFyjlxtTzAeyOXHK5pcYcPljz-Wm40wSEIkRL6vE-l7jE5QLt_yDXa-qXUVseUsePnL32XTPg@candidate.19.mongolayer.com:11619,candidate.18.mongolayer.com:11781/app51641744"
echo "Starting the server.";
echo $MONGODB_URL
echo "Starting the server.";
echo "Installing node modules.";
npm install
SUCCESS=`echo $?`
if [[ ("$SUCCESS" == 0) ]] ;  then
        echo "Successfully installed node modules"
else
        echo "Not able to install node modules. Exiting"
        exit 11
fi

echo "Stopping the server"
forever stop  /home/ubuntu/app/lms/app/server.js

sleep 5;

forever stop  /home/ubuntu/app/lms/app/server.js

sleep 5;
echo "Successfully stopped the server."

echo "Rotating lms.log"
ls /home/ubuntu/.forever/lms.log
SUCCESS=`echo $?`
if [[ ("$SUCCESS" == 0) ]] ;  then
        mv /home/ubuntu/.forever/lms.log /home/ubuntu/.forever/lms.log.`date +%s`
fi

echo "Starting the server"
nohup forever -a -l lms.log  start /home/ubuntu/app/lms/app/server.js &
sleep 10;

grep "Successfully connected" /home/ubuntu/.forever/lms.log
SUCCESS=`echo $?`
if [[ ("$SUCCESS" == 0) ]] ;  then
        echo "Server started successfully"
else
        echo "Server failed to start. Please check this."
        #TODO.. Send email.
fi

wget -qO - localhost:3002/health  --timeout=5 -t 1 | grep '{success: 1}'
SUCCESS=`echo $?`
if [[ ("$SUCCESS" == 0) ]] ;  then
        echo "Server is ready to server connections"
else
        echo "Server failed to connect to DB and serve connections. Exiting."
        #TODO.. Send email.
        exit 13
fi

echo "All done.. exiting."
exit 0
