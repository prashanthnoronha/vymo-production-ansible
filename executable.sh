!#/bin/sh
ansible-playbook vpc.yml -i inventory/hosts

#one manual step to unset the routable as not main
ansible-playbook -i inventory/hosts routeCreation.yml

#add [bastion]
ansible-playbook -i inventory/hosts bastion.yml

#add [webserver]
ansible-playbook -i inventory/hosts nodeInstanceCreation.yml

#Install Stuff on the webserver
ansible-playbook -i inventory/hosts nodeServerDeploy.yml --extra-vars '{"NODE_ENV":"development","NODEJS_ACTION":"notyet","REPOUPDATE":"true","INSTALL":"true"}' -vvvvvvv

#Start Node Server
ansible-playbook -i inventory/hosts nodeServerDeploy.yml --extra-vars '{"NODE_ENV":"development","NODEJS_ACTION":"start","REPOUPDATE":"false","INSTALL":"false"}'

#TODO create the weservers to be under a specific ILB & ERROR! vars file secret_vars/VymoWebservers.yml was not found
ansible-playbook -i inventory/hosts internalLoadBalancer.yml -vvvvvv

#Just create the RP nodes
ansible-playbook -i inventory/hosts reverseProxy.yml

#To create internal Load Balancers
ansible-playbook -i inventory/hosts internalLoadBalancer.yml

#Create a ROUTE 53 name - take the Zone id of the ELB out from ELB cretation step
ansible-playbook -i inventory/hosts dns.yml --extra-vars '{"VPC_DNS_ZONE":"getVymo.com", "CLIENT":"hdfc", "zoneId":"Z3DZXE0Q79N41H"}' -vvvvvvvvvvv



