
#Create the VPC
ansible-playbook vpc.yml -i inventory/hosts

#sudo pip install awscli
ansible-playbook natgateway.yml -i inventory/hosts

#Create NAT GATEWAY Proofs
ansible-playbook routeCreation.yml -i inventory/hosts

#Manually set the private route as the main ROUTE

aws ec2 create-key-pair --key-name Vym02o13PRODUCTION.pem --region us-east-1

#Create Security Groups
ansible-playbook securityGroups.yml  -i inventory/hosts

#Create Bastion
ansible-playbook bastion.yml  -i inventory/hosts

#Create the Node Instances
ansible-playbook -i inventory/hosts nodeInstanceCreation.yml

#Install LMS Stuff onto Node
ansible-playbook -i inventory/hosts nodeServerDeploy.yml --extra-vars '{"NODE_ENV":"development","NODEJS_ACTION":"notyet","repoinstall":"true","INSTALL":"true"}'

#Start the node server
ansible-playbook -i inventory/hosts nodeServerDeploy.yml --extra-vars '{"NODE_ENV":"development","NODEJS_ACTION":"start","REPOUPDATE":"false","INSTALL":"false"}'

#Create the ILB and add the Node instances to it
ansible-playbook -i inventory/hosts internalLoadBalancer.yml

#Create Reverse Proxy Instances add [reverseProxy] to the inventory/hosts file
ansible-playbook -i inventory/hosts reverseProxy.yml

#Deploy nginx and its configs - TODO replace the LB URL automatically
ansible-playbook -i inventory/hosts configureNginx.yml

#External Load Balancer
ansible-playbook -i inventory/hosts externalLoadBalancer.yml

#resiliency
ansible-playbook -i inventory/hosts resilience.yml